﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Assessment2
{
    class Program
    {
        static public double[] First = new double[10];
        static public double[] Array3 = new double[0];
        static public string[] Array5 = new string[0];
        static public string Pop;
        static public int Question;
        static public double More;
        static public int[] MoreIndices;
        static public double Less;
        static public int[] LessIndices;
        static public double Value;
        static public int[] VIndices;
        static void Main(string[] args)
        {
            do
            {

                Console.WriteLine("~~~~~~~~~~~~~~~~~~~Welcome to Assessment 2~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~of~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~Jacob~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~   Choose any number for diffrent programs :-  ~~~~~~~");
                Console.WriteLine("~~~~~~~   1.To find the top 10 value of a text file   ~~~~~~~");
                Console.WriteLine("~~~~~~~   2.Selection sort method                     ~~~~~~~");
                Console.WriteLine("~~~~~~~   3.Linear Search                             ~~~~~~~");
                Console.WriteLine("~~~~~~~   4.Bubble sort method                        ~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                var read = Console.ReadLine();
                if (read == "1")
                {
                    Console.Clear();
                    importData();
                    printArray(Array3);
                    Console.WriteLine("To find the 10 values of the text file:\n");
                    First = findMaximum();
                    Question = 1;
                    printArray(First);
                    Console.WriteLine("Repeat the program, 'y' for yes, 'n' for no");
                    Pop = Console.ReadLine();
                    Console.Clear();

                }
                else if (read == "2")
                {
                    Console.Clear();
                    Question = 2;
                    importData();
                    Console.WriteLine("Unsorted:\n");
                    printArray(Array3);
                    Console.WriteLine("Press 'Enter' to view sorted File");
                    Console.ReadLine();
                    Console.WriteLine("Sorted:\n");
                    double[] sortedArr = selectionSort(Array3);
                    printArray(sortedArr);
                    Console.WriteLine("Press 'Enter' to see the maximum, minimum and average of the values");
                    Console.ReadLine();

                    More = sortedArr[sortedArr.Length - 1];
                    Less = sortedArr[0];
                    Value = 0;

                    for (int i = 0; i < sortedArr.Length; i++)
                    {
                        Value += sortedArr[i];
                    }

                    Value = Value / sortedArr.Length;

                    FindValues();
                    Console.WriteLine();
                    Console.WriteLine("Repeat the program, 'y' for yes, 'n' for no");
                    Pop = Console.ReadLine();
                    Console.Clear();
                }
                else if (read == "3")
                {
                    Console.Clear();
                    Question = 3;
                    importData();
                    linearSearch();
                    Console.WriteLine("Press 'Enter' to check the time of maximum, minimum and average of the values ");
                    Console.ReadLine();
                    compareArrays();
                    Console.WriteLine("\nFor repeat the program, 'y' for yes, 'n' for no");
                    Pop = Console.ReadLine();
                    Console.Clear();
                }
                else if (read == "4")
                {
                    Console.Clear();
                    Question = 4;
                    BubbleSortTiming bubbleSorting = new BubbleSortTiming();
                    bubbleSorting.bubbleSort();
                    bubbleSorting.improvedBubbleSort();
                    Console.WriteLine("\nFor repeat the program, 'y' for yes, 'n' for no");
                    Pop = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Command again");
                    Pop = "y";
                }
            } while (Pop == "y");

            if (Pop == "n")
            {

                Console.WriteLine("~~~~~   Thankx see u again  ~~~~~");

            }
            else
            {
                Console.WriteLine("Restart the program");
            }
            Console.Clear();
        }
        static public void importData()
        {
            double temp = 0;
            int counter = 0;
            string line;

            Array3 = null;
            Array5 = null;

            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");
            while ((line = file.ReadLine()) != null)
            {
                double.TryParse(line, out temp);
                Array.Resize(ref Array3, counter + 1);
                Array3[counter] = temp;
                counter++;
            }
            file.Close();

            if (Question == 3)
            {
                StreamReader file2 = new StreamReader(@"c:\DateTime_Data.txt");
                counter = 0;
                while ((line = file2.ReadLine()) != null)
                {
                    Array.Resize(ref Array5, counter + 1);
                    Array5[counter] = line;
                    counter++;
                }
                file2.Close();
            }
        }
        static public double[] findMaximum()
        {
            double[] ArrTemp = new double[Array3.Length];
            Array.Copy(Array3, ArrTemp, Array3.Length);

            double[] maxArray = new double[10];
            int maxIndex = 0;

            for (int i = 0; i < 10; i++)
            {
                maxArray[i] = ArrTemp[0];

                for (int j = 1; j < ArrTemp.Length; j++)
                {
                    if (maxArray[i] < ArrTemp[j])
                    {
                        maxArray[i] = ArrTemp[j];
                        maxIndex = j;
                    }
                }

                ArrTemp[maxIndex] = 0;
            }

            return maxArray;
        }

        static public void printArray(double[] tempArr)
        {
            for (int i = 1; i <= tempArr.Length; i++)
            {
                Console.Write(tempArr[i - 1] + ",  ");
                if (i % 10 == 0)
                    Console.WriteLine();
            }

            Console.WriteLine();
        }

        public static double[] selectionSort(double[] data)
        {
            int max;
            double temp;

            for (int i = 0; i < data.Length - 1; i++)
            {
                max = i;
                for (int j = i + 1; j < data.Length; j++)
                {
                    if (data[j] > data[max])
                    {
                        max = j;
                    }
                }

                if (max != 1)
                {
                    temp = data[i];
                    data[i] = data[max];
                    data[max] = temp;
                }
            }

            return data;
        }

        static public void FindValues()
        {
            Console.WriteLine("Max:   " + More);
            Console.WriteLine("Min:   " + Less);
            Console.WriteLine("Value: " + Value);

        }
        static public void linearSearch()
        {
            MoreIndices = null;
            LessIndices = null;
            VIndices = null;

            int count = 0;
            Console.WriteLine("Indice of the maximum values are: ");
            for (int i = 0; i < Array3.Length; i++)
            {
                if (Array3[i] == More)
                {
                    Console.Write(i + " ");
                    Array.Resize(ref MoreIndices, count + 1);
                    MoreIndices[count] = i;
                    ++count;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Indices of the minimum values are: ");

            count = 0;
            for (int i = 0; i < Array3.Length; i++)
            {
                if (Array3[i] == Less)
                {
                    Console.Write(i + " ");
                    Array.Resize(ref LessIndices, count + 1);
                    LessIndices[count] = i;
                    ++count;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Indices of the avarage values are: ");

            count = 0;
            for (int i = 0; i < Array3.Length; i++)
            {
                if (Array3[i] == Value)
                {
                    Console.Write(i + " ");
                    Array.Resize(ref VIndices, count + 1);
                    VIndices[count] = i;
                    ++count;
                }
            }

            if (VIndices == null)
            {
                Console.WriteLine("No data");
            }
        }
        static public void compareArrays()
        {
            Console.WriteLine("Occurrence of maximum value: ");
            Console.WriteLine(MoreIndices.Length);
            for (int i = 0; i < MoreIndices.Length; i++)
            {
                Console.WriteLine(Array5[MoreIndices[i]]);
            }

            Console.WriteLine();
            Console.WriteLine("Occurrence of minimum value: ");

            for (int i = 0; i < LessIndices.Length; i++)
            {
                Console.WriteLine(Array5[LessIndices[i]]);
            }

            Console.WriteLine("Occurrence of avarage value: ");
            Console.WriteLine();

            if (VIndices != null)
            {
                for (int i = 0; i < VIndices.Length; i++)
                {
                    Console.WriteLine(Array5[VIndices[i]]);
                }
            }
        }
    }
    class BubbleSortTiming
    {
        public double temp = 0;
        public int counter = 0;
        public string line;
        public double[] arr = new double[0];
        Stopwatch st = new Stopwatch();
        public void bubbleSort()
        {

            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");
            st.Start();
            while ((line = file.ReadLine()) != null)
            {
                double.TryParse(line, out temp);
                Array.Resize(ref arr, counter + 1);
                arr[counter] = temp;
                counter++;
            }
            file.Close();
            double[] NorArr = new double[arr.Length];
            Array.Copy(arr, NorArr, arr.Length);
            for (int write = 0; write < NorArr.Length; write++)
            {
                for (int sort = 0; sort < NorArr.Length - 1; sort++)
                {
                    if (NorArr[sort] > NorArr[sort + 1])
                    {
                        temp = NorArr[sort + 1];
                        NorArr[sort + 1] = NorArr[sort];
                        NorArr[sort] = temp;
                    }
                }
            }
            st.Stop();
            Program.printArray(NorArr);
            Console.WriteLine("Bubble sort as normal: {0}", st.Elapsed);
            Console.WriteLine();
        }
        public void improvedBubbleSort()
        {
            double[] bubble1Arr = new double[arr.Length];
            Array.Copy(arr, bubble1Arr, arr.Length);
            double[] bubble2Arr = new double[arr.Length];
            Array.Copy(arr, bubble2Arr, arr.Length);
            st.Reset();
            st.Start();
            for (int write = 0; write < bubble1Arr.Length; write++)
            {
                int test = 0;

                for (int sort = 0; sort < bubble1Arr.Length - 1 - write; sort++)
                {
                    if (bubble1Arr[sort] > bubble1Arr[sort + 1])
                    {
                        temp = bubble1Arr[sort + 1];
                        bubble1Arr[sort + 1] = bubble1Arr[sort];
                        bubble1Arr[sort] = temp;
                        test = 1;
                    }
                }
                if (test == 0)
                {
                    break;
                }
            }
            st.Stop();
            Program.printArray(bubble1Arr);
            Console.WriteLine("\nBubble sort as improved takes: {0}", st.Elapsed);

        }
    }
}